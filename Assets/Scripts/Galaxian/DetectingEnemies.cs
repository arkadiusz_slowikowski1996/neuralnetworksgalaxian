﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Galaxian
{
    public delegate void Detecting<T>(T target_);

    public class DetectingEnemies : MonoBehaviour
    {
        private static DetectingEnemies instance;

        public static List<EnemyBot> enemiesInside = new List<EnemyBot>();
        public List<EnemyBot> debug = new List<EnemyBot>();

        private void Awake()
        {
            instance = this;
        }

        private void Update()
        {
            debug = enemiesInside;
        }

        private void OnTriggerEnter(Collider collision)
        {
            EnemyBot _enemy = collision.gameObject.GetComponent<EnemyBot>();
            if (_enemy && !enemiesInside.Contains(_enemy))
                enemiesInside.Add(_enemy);
        }

        private void OnTriggerExit(Collider collision)
        {
            Debug.Log(collision.gameObject.name);
            EnemyBot _enemy = collision.gameObject.GetComponent<EnemyBot>();
            if (enemiesInside.Contains(_enemy))
                enemiesInside.Remove(_enemy);
        }

        public static void DeleteMe(EnemyBot enemyBot_)
        {
            if (enemiesInside.Contains(enemyBot_))
                enemiesInside.Remove(enemyBot_);
        }

        //public static event Detecting<EnemyBot> EnemyDetected;

        //private void OnTriggerEnter2D(Collider2D other)
        //{
        //	EnemyBot _enemy = other.GetComponent<EnemyBot>();
        //	if (_enemy)
        //		EnemyDetected?.Invoke(_enemy);
        //}
    }
}