﻿using System.IO;
using System.Text;
using UnityEngine;

namespace Galaxian
{
    public class GameMode : MonoBehaviour
    {
        private static GameMode instance;

        public bool save;

        public static bool Save { get { return instance.save; } }

        private void Awake()
        {
            instance = this;
        }

        public static double[] LoadWeightsFromFile()
        {
            double[] _returnValue = null;//NeuralNetwork.CreateEmptyWeightsArray();

            string _fileContent = File.ReadAllText(Application.dataPath + @"\Weights.txt");
            string[] _fileContentSeperated = _fileContent.Split('|');

            _returnValue = new double[_fileContentSeperated.Length];

            for (int i = 0; i < _returnValue.Length; i++)
                _returnValue[i] = double.Parse(_fileContentSeperated[i]);

            return _returnValue;
        }
    }
}