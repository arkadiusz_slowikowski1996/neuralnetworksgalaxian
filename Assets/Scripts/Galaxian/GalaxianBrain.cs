﻿using GeneticNeuralNetwork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Galaxian
{
    public class GalaxianBrain : Brain
    {
        public delegate void Reseting();
        public static event Reseting Reset;
        public delegate void Dying();
        public static event Dying Died;

        public bool debug;

        public Shooting playerShooting;
        public Movement playerMoving;
        public bool trainingDummy = true;
        [SerializeField] private Text debugText;
        [SerializeField] private float shotDelay = 0.2f;
        [SerializeField] private float shootingTrashhold = 0.5f;
        private float shotsTimer;
        private double shooting = 0;
        private double moving = 0;
        private Vector3 startPos;

        public int timeScale;

        public double[] outputs;
        double[] inputs;

        void Start()
        {
            Time.timeScale = timeScale;

            outputs = new double[GlobalSettings.NNoutputs];
            inputs = new double[GlobalSettings.NNinputs];

            startPos = transform.position;
        }

        void FixedUpdate()
        {
            if (!Manager.Target) return;
            double _positionDifference = (transform.position.x - Manager.Target.transform.position.x);
            double _enemyVelocity = (Manager.Target.movement.XVel);

            if (debug) Debug.Log(_enemyVelocity);

            inputs[0] = _positionDifference;
            inputs[1] = _enemyVelocity;
            inputs[2] = Manager.Target.transform.position.y;

            outputs = network.Process(inputs);
            shooting = outputs[0];
            moving = outputs[1];

            playerMoving.Move((float)moving);

            shotsTimer += Time.fixedDeltaTime;
            if (shotsTimer >= shotDelay)
            {
                if (shooting > shootingTrashhold)
                {
                    playerShooting.Fire(this, _positionDifference, _enemyVelocity);
                    shotsTimer = 0;
                }
            }
        }

        public void ShotShot(bool good)
        {
            if (trainingDummy)
            {
                if (good)
                {
                    //points += 20;
                    ChangeScore(1);
                }
                else
                {
                    //points++;
                    ////Die();
                }
            }
            else
            {
                TestingManager.Hit(good);
            }
        }

        public void GetHit()
        {
            if (trainingDummy)
            {
                ChangeScore(-1);
            }
        }

        bool isdead;
        public void Die()
        {
            if (!isdead)
            {
                isdead = true;
                Died?.Invoke();
                try
                {
                    Destroy(transform.parent.gameObject);
                }
                catch
                {
                    Debug.Log("Destroying error");
                }
            }
        }
    }
}