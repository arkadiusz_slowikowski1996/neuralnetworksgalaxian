﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Galaxian
{
	public class Bullet : MonoBehaviour
	{
		public static List<Bullet> enemyBullets = new List<Bullet>();

		public bool createdByPlayer;

		private bool trainUponMe;

		[SerializeField] private new Rigidbody rigidbody;
		[SerializeField] private new Renderer renderer;
		[SerializeField] private Color createdByPlayer_color;
		[SerializeField] private Color notCreatedByPlayer_color;

		private GalaxianBrain creator;
		private float speed;
		private double positionDifference = 2137;
		private double enemyVelocity = 2137f;

		private void Start()
		{
			Destroy(gameObject, 5);
		}

		public static Bullet TheClosestBulletToMe(Vector3 myPos_)
		{
			float _smolestDist = float.MaxValue;
			int _winner = 0;

			for (int i = 0; i < enemyBullets.Count; i++)
			{
				float _dist = Vector3.Distance(enemyBullets[i].transform.position, myPos_);
				if (_dist < _smolestDist)
				{
					_winner = i;
					_smolestDist = _dist;
				}
			}

			return enemyBullets.Count > 0? enemyBullets[_winner] : null;
		}

		public void Setup(GalaxianBrain creator_, Vector2 direction_, float speed_, double positionDifference_, double enemyVelocity_)
		{
			speed = speed_;
			creator = creator_;
			rigidbody.velocity = speed * direction_;
			renderer.material.color = direction_.y > 0 ? createdByPlayer_color : notCreatedByPlayer_color;
			enemyBullets.Add(this);
			createdByPlayer = direction_.y > 0;
			positionDifference = positionDifference_;
			enemyVelocity = enemyVelocity_;
			trainUponMe = true;
		}

		public void Setup(Vector2 direction_, float speed_)
		{
			speed = speed_;
			rigidbody.velocity = speed * direction_;
			renderer.material.color = direction_.y > 0 ? createdByPlayer_color : notCreatedByPlayer_color;
			createdByPlayer = direction_.y > 0;
			trainUponMe = false;
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.tag == "Boundary")
			{
				Destroy(gameObject);
				enemyBullets.Remove(this);

				if (trainUponMe)
					new GalaxianTrainingData(positionDifference, enemyVelocity, false).SendTrainingData(creator);
			}

			if (createdByPlayer && other.tag == "Enemy")
			{
				other.GetComponent<Health>().GetHit(1);
				Destroy(gameObject);
				enemyBullets.Remove(this);

				if (trainUponMe)
					new GalaxianTrainingData(positionDifference, enemyVelocity, true).SendTrainingData(creator);
			}

			if (!createdByPlayer && other.tag == "Player")
			{
				//other.GetComponent<Health>().GetHit(1);
				Destroy(gameObject);
				enemyBullets.Remove(this);
			}
		}
	}
}