﻿using GeneticNeuralNetwork;
using System;
using System.Collections;
using UnityEngine;
namespace Galaxian
{
    public class TestingManager : Manager
    {
        private static TestingManager instance;

        private Brain mainBrain;
        private int hits;
        private int misses;

        private int numberOfDead;

        private new void Awake()
        {
            base.Awake();
            instance = this;
        }

        private void _Start()
        {
            base.Awake();
        }

        private void EnemyBot_Died()
        {
            numberOfDead++;
            Debug.Log(numberOfDead + " " + enemies.Length);
            if (numberOfDead == enemies.Length)
            {
                SpawnEnemies(false);
                numberOfDead = 0;
            }
        }

        private void Update()
        {
            ChooseTarget();

            debugText.text = string.Format("Hits: {0}, Misses: {1}", hits, misses);
        }

        private void ChooseTarget()
        {
            ///Get first alive
            int i = 0;
            for (; i < enemies.Length; i++)
            {
                if (enemies[i])
                {
                    target = enemies[i];
                    break;
                }
            }

            float _targetsDistance = float.MinValue;

            for (; i < enemies.Length; i++)
            {
                if (!enemies[i]) continue;
                float _distance = Vector3.Distance(enemies[i].transform.position, mainBrain.transform.position);
                if (_distance < _targetsDistance)
                {
                    target = enemies[i];
                    _targetsDistance = _distance;
                }
            }
        }

        private void SpawnBrains()
        {
            mainBrain = Instantiate(brainPrefab);
            mainBrain.network = new NeuralNetwork();
            mainBrain.network.CopyNetworkFrom(GeneticManager.theBestNetwork);
            mainBrain.transform.position = brainDefaultPosition.position;
        }

        public static void Hit(bool successful_)
        {
            if (instance.gameObject.activeInHierarchy)
            {
                if (successful_)
                    instance.hits++;
                else
                    instance.misses++;
            }
        }

        public override void OnActivated()
        {
            NeuralNetwork.Setup(NNinputs, NNoutputs, NNhiddenLayers, NNhiddenNeurons);
            SpawnBrains();
            SpawnEnemies(false);
            _Start();
            EnemyBot.Died += EnemyBot_Died;
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();
            hits = 0;
            misses = 0;
            EnemyBot.Died -= EnemyBot_Died;

            if (mainBrain) Destroy(mainBrain.gameObject);
        }
    }
}