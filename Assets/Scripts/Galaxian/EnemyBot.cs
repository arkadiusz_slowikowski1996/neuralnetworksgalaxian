﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Galaxian
{
    public class EnemyBot : MonoBehaviour
    {
        [HideInInspector] public bool manual;

        private delegate void TouchingBoundry();
        private static event TouchingBoundry BoundryTouched;
        public delegate void Dying();
        public static event Dying Died;

        public Movement movement;
        [SerializeField] private Health hp;

        [SerializeField] private Shooting shooting;
        [SerializeField] private float minShootDelay;
        [SerializeField] private float maxShootDelay;

        private int dir = 1;
        private static bool wasCalled;

        float prevX;

        private void Start()
        {
            GalaxianBrain.Reset += GalaxianBrain_Reset;
            BoundryTouched += EnemyMovement_BoundryTouched;
            if (shooting) StartCoroutine(Shooting());
        }

        private void FixedUpdate()
        {
            if (manual)
            {
                movement.Move(Input.GetAxis("Horizontal"));
            }
            else
            {
                movement.Move(dir);
                wasCalled = false;
            }

            prevX = transform.position.x;
        }

        //private void OnCollisionEnter(Collision collision)
        //{
        //    Debug.Log("OnCollisionEnter");
        //    if (!wasCalled && collision.gameObject.tag == "Boundary")
        //    {
        //        Debug.Log(collision.gameObject);
        //        if (BoundryTouched != null)
        //        {
        //            BoundryTouched();
        //            wasCalled = true;
        //        }
        //    }
        //}

        private void OnTriggerEnter(Collider other)
        {
            if (!wasCalled && other.tag == "Boundary")
            {
                if (BoundryTouched != null)
                {
                    BoundryTouched();
                    wasCalled = true;
                }
            }
        }

        public void Die()
        {
            Debug.Log("Enemy died");
            Died?.Invoke();
        }

        public void GalaxianBrain_Reset()
        {
            MoveOnPath moveOnPath = GetComponent<MoveOnPath>();
            if (moveOnPath) moveOnPath.GalaxianBrain_Reset();
        }

        private IEnumerator Shooting()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(minShootDelay, maxShootDelay));

                shooting.Fire();
            }
        }

        private void EnemyMovement_BoundryTouched()
        {
            dir *= -1;
        }

        private void OnDestroy()
        {
            BoundryTouched -= EnemyMovement_BoundryTouched;
            DetectingEnemies.DeleteMe(this);
        }
    }
}