﻿using GeneticNeuralNetwork;
using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;

namespace Galaxian
{
    [Serializable]
    public class EnemyPositions
    {
        public Transform[] poses;
    }

    public class TrainingManager : Manager
    {
        private static TrainingManager instance;

        [SerializeField] private EnemyPositions[] poses;
        [SerializeField] private int numberOfParents = 1;
        [SerializeField] private int numberOfMutations = 1;
        [SerializeField] private float runTime = 10;

        private Transform CurrStartPos { get { return poses[posIndex].poses[posIndexSec]; } }
        private Transform CurrFinalPos { get { return poses[posIndex].poses[(posIndexSec + 1) % poses[posIndex].poses.Length]; } }

        private float currTime;
        private int posIndex = 0;
        private int posIndexSec = 0;
        private int numberOfDead;
        private int currGeneration;
        private int PosIndexSec
        {
            get { return posIndexSec; }
            set
            {
                if (posIndexSec == 1)
                {
                    posIndex = ++posIndex % poses.Length;
                    posIndexSec = 0;
                }
                else
                    posIndexSec = value;
            }
        }

        [SerializeField] private float timeScale = 1;

        private new void Awake()
        {
            base.Awake();
            instance = this;
        }

        private void _Start()
        {
            base.Awake();
            GeneticManager.Setup( numberOfParents, numberOfMutations);
            target = enemies[0];
            StartCoroutine(KillBrainsAfterRunTime());
        }

        private IEnumerator KillBrainsAfterRunTime()
        {
            target.GetComponent<Health>().hp = int.MaxValue;
            yield return new WaitForSeconds(runTime);

            GeneticManager.CreateNewGeneration(brainPrefab, BrainsPopulation);
            currTime = 0;
            PosIndexSec++;
            currGeneration++;
            StartCoroutine(KillBrainsAfterRunTime());
        }

        private void Update()
        {
            if (instance)
                debugText.text = string.Format("Number of alives: {0}, generation: {1}", BrainsPopulation - numberOfDead, currGeneration, BrainsPopulation);
            MoveTarget();

            Time.timeScale = timeScale;
        }

        private void MoveTarget()
        {
            target.transform.position = Vector3.Lerp(
                CurrStartPos.position,
                CurrFinalPos.position, currTime / runTime);

            currTime += Time.deltaTime;
        }

        public override void OnActivated()
        {
            NeuralNetwork.Setup(NNinputs, NNoutputs, NNhiddenLayers, NNhiddenNeurons);
            GeneticManager.CreateNewGeneration(brainPrefab, BrainsPopulation);
            SpawnEnemies(true);
            _Start();
        }

        public override void OnDeactivated()
        {
            base.OnDeactivated();

            numberOfDead = 0;
            currGeneration = 0;

            GeneticManager.DestoryBrainObjects();
        }
    }
}