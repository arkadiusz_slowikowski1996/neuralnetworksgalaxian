﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian
{
    [Serializable]
    public struct GalaxianTrainingData
    {
        private static List<GalaxianTrainingData> listOfData = new List<GalaxianTrainingData>();

        public double positionDifference;
        public double enemyVelocity;
        public bool good;

        public List<double> Inputs
        {
            get
            {
                List<double> _returnValue = new List<double>();
                _returnValue.Add(positionDifference);
                _returnValue.Add(enemyVelocity);
                return _returnValue;
            }
        }

        public GalaxianTrainingData(double positionDifference_, double enemyVelocity_, bool good_)
        {
            positionDifference = positionDifference_ / 4;
            enemyVelocity = enemyVelocity_ / 3;
            good = good_;
        }

        public void SendTrainingData(GalaxianBrain to_)
        {
            //NeuralNetwork.TrainS(Inputs, _outputs);
            to_?.ShotShot(good);

            listOfData.Add(this);
        }

        public static string ToJSON()
        {
            GalaxianTrainingData_List galaxianTrainingData_List = new GalaxianTrainingData_List()
            {
                list = listOfData
            };

            return JsonUtility.ToJson(galaxianTrainingData_List);
        }

        public static void FromJSON(string json_)
        {
            listOfData.AddRange(JsonUtility.FromJson<GalaxianTrainingData_List>(json_).list);

            List<GalaxianTrainingData> newList = JsonUtility.FromJson<GalaxianTrainingData_List>(json_).list;

            //foreach (GalaxianTrainingData gtd in newList)
            //gtd.SendTrainingData();
        }
    }

    [Serializable]
    public class GalaxianTrainingData_List
    {
        public List<GalaxianTrainingData> list;
    }
}