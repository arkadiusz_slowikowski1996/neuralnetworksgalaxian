﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Galaxian
{
    public class MoveOnPath : MonoBehaviour
    {
        [SerializeField] public PathSegment[] path;
        [SerializeField] private Movement movement;

        private int curr;
        private Coroutine moveCoroutine;

        private void OnEnable()
        {
            moveCoroutine = StartCoroutine(Move());
        }

        private void Start()
        {
            GalaxianBrain.Reset += GalaxianBrain_Reset;
        }

        public void GalaxianBrain_Reset()
        {
            curr = 0;
            PathSegment temp = path[0];
            path[0] = path[2];
            path[2] = temp;
            StopCoroutine(moveCoroutine);
            moveCoroutine = StartCoroutine(Move());
        }

        private IEnumerator Move()
        {
            while (true)
            {
                float _diff = path[curr].transform.position.x - transform.position.x;
                movement.Move(_diff > 0 ? 1 : -1);

                if (Mathf.Abs(_diff) < 0.05f)
                {
                    movement.rigidbody.MovePosition(path[curr].transform.position);
                    yield return new WaitForSeconds(path[curr].time);
                    curr++;
                    curr %= path.Length;
                }

                yield return 0;
            }
        }
    }

    [Serializable]
    public class PathSegment
    {
        public Transform transform;
        public float time;
    }
}