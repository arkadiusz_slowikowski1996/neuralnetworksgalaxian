﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian
{
	public class Health : MonoBehaviour
	{
		[SerializeField] private GalaxianBrain galaxianBrain;
		[SerializeField] private EnemyBot enemyBot;

		public int hp;

		public void GetHit(int dmg_)
		{
			if (galaxianBrain)
				galaxianBrain.GetHit();
            hp -= dmg_;

            if (hp <= 0)
            {
                Destroy(gameObject);
                enemyBot.Die();
            }
        }
	}
}