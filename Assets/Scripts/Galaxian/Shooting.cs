﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian
{
    public class Shooting : MonoBehaviour
    {
        public float bulletsSpeed;

        [SerializeField] private Bullet bulletPrefab;
        [SerializeField] private Transform bulletsOrigin;

        public void Fire()
        {
            CreateBullet();
        }

        public void Fire(GalaxianBrain creator_, double positionDifference_, double enemyVelocity_)
        {
            CreateBullet(creator_, positionDifference_, enemyVelocity_);
        }

        private void CreateBullet()
        {
            Bullet _bullet = Instantiate(
            bulletPrefab,
            bulletsOrigin.position,
            bulletPrefab.transform.rotation,
            null
            );

            Vector2 _dir = bulletsOrigin.localPosition;

            _bullet.Setup(_dir, bulletsSpeed);
        }

        private void CreateBullet(GalaxianBrain creator_, double positionDifference_, double enemyVelocity_)
        {
            Bullet _bullet = Instantiate(
            bulletPrefab,
            bulletsOrigin.position,
            bulletPrefab.transform.rotation,
            null
            );

            Vector2 _dir = bulletsOrigin.localPosition;

            _bullet.Setup(creator_, _dir, bulletsSpeed, positionDifference_, enemyVelocity_);
        }
    }
}