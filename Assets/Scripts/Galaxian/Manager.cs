﻿using GeneticNeuralNetwork;
using UnityEngine;
using UnityEngine.UI;

namespace Galaxian
{
    public abstract class Manager : BaseManager
    {
        private static Manager instance;

        [SerializeField] protected Brain brainPrefab;
        [SerializeField] protected Transform brainDefaultPosition;
        [SerializeField] private Transform brainTransform;
        [SerializeField] protected Text debugText;
        [SerializeField] public EnemyBot target;
        [SerializeField] private EnemyBot enemyPrefab;
        [SerializeField] protected Transform enemiesTransform;
        [SerializeField] private int brainsPopulation;
        [SerializeField] protected int enemiesHP = 1;
        [SerializeField] protected int enemiesRows = 1;
        [SerializeField] protected int enemiesColumns = 1;
        [SerializeField] protected float rowsStartY;
        [SerializeField] protected float rowsEndY;
        [SerializeField] protected float columsStartX;
        [SerializeField] protected float columnsEndX;
        [SerializeField] protected int NNinputs;
        [SerializeField] protected int NNoutputs;
        [SerializeField] protected int NNhiddenLayers;
        [SerializeField] protected int NNhiddenNeurons;
        protected EnemyBot[] enemies;

        public static int BrainsPopulation { get { return instance.brainsPopulation; } }
        public static EnemyBot Target { get { return instance.target; } }

        protected void Awake()
        {
            instance = this;
        }

        public override void OnDeactivated()
        {
            if (enemies != null)
            {
                for (int i = 0; i < enemies.Length; i++)
                    if (enemies[i]) Destroy(enemies[i].gameObject);

                enemies = null;
            }
        }

        protected void SpawnEnemies(bool letManagerSteer_)
        {
            enemies = new EnemyBot[enemiesColumns * enemiesRows];
            float _xDist = (columnsEndX - columsStartX) / (enemiesColumns + 1);
            float _yDist = (rowsEndY - rowsStartY) / (enemiesRows + 1);

            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = Instantiate(enemyPrefab, enemiesTransform);
                enemies[i].transform.localPosition = new Vector2(
                    (_xDist * ((i % enemiesColumns) + 1)) + columsStartX,
                    (_yDist * ((i % enemiesRows) + 1)) + rowsStartY
                    );
                enemies[i].manual = letManagerSteer_;
            }
        }
    }
}