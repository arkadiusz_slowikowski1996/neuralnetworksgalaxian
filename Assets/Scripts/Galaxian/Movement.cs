﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian
{
	public class Movement : MonoBehaviour
	{
		public new Rigidbody rigidbody;
		[SerializeField] private float speed;

		private float prevX;

		public float XVel
		{
			get { return transform.position.x - prevX; }
		}

		private void FixedUpdate()
		{
			//if(name == "Enemy")
			//Debug.Log(XVel);



			prevX = transform.position.x;
		}

		public void Move(float horizontal_)
		{
			Vector3 _finalPos = Vector3.zero;
			_finalPos += horizontal_ * transform.right;

			_finalPos *= speed * Time.fixedDeltaTime;
			_finalPos += rigidbody.position;

			rigidbody.MovePosition(new Vector3(
				Mathf.Clamp(_finalPos.x, GlobalSettings.minX, GlobalSettings.maxX),
				Mathf.Clamp(_finalPos.y, GlobalSettings.minY, GlobalSettings.maxY),
				0));
		}
	}
}