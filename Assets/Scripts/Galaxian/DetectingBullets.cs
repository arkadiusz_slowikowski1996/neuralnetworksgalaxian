﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Galaxian
{
    public class DetectingBullets : MonoBehaviour
    {
        public static event Detecting<Bullet> BulletDetected;

        private void OnTriggerEnter(Collider other)
        {
            Bullet _bullet = other.GetComponent<Bullet>();
            if (_bullet)
                BulletDetected?.Invoke(_bullet);
        }
    }
}