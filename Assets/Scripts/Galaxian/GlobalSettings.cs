﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Galaxian
{
    public static class GlobalSettings
    {
        public static float minX = 0;
        public static float maxX = 20;
        public static float minY = 0;
        public static float maxY = 15;

        public static int NNinputs = 3;
        public static int NNhiddenLayers = 1;
        public static int NNhiddenNeurons =5;
        public static int NNoutputs = 2;
    }
}