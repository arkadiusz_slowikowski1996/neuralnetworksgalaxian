﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Car
{
    public class Wall : MonoBehaviour
    {
        [SerializeField] private bool isFinish;

        private void OnCollisionEnter(Collision collision)
        {
            collision.rigidbody.GetComponent<CarBrain>().TurnOff(isFinish);
        }
    }
}