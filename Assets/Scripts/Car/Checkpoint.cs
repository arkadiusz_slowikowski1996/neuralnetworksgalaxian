﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
	private static List<Checkpoint> instances = new List<Checkpoint>();

	private void Awake()
	{
		instances.Add(this);
	}

	private void OnDestroy()
	{
		instances.Remove(this);
	}


}