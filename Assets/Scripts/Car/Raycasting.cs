﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Car
{
    public class Raycasting : MonoBehaviour
    {
        public RaycastHit[] raycastHits;

        [SerializeField] private Transform raycastOrigin;

        private Vector3[] raycastDirs;
        private int layerMask;

        [SerializeField] private float[] distances;

        private void Start()
        {
            raycastDirs = new Vector3[5];
            distances = new float[raycastDirs.Length];
            raycastHits = new RaycastHit[raycastDirs.Length];

            layerMask = ~(1 << LayerMask.GetMask("Car") | 1 << LayerMask.GetMask("Default"));
        }

        private void Update()
        {
            raycastDirs[0] = raycastOrigin.TransformDirection(new Vector3(1, 0, 0));
            raycastDirs[1] = raycastOrigin.TransformDirection(new Vector3(1, 0, 1));
            raycastDirs[2] = raycastOrigin.TransformDirection(new Vector3(0, 0, 1));
            raycastDirs[3] = raycastOrigin.TransformDirection(new Vector3(-1, 0, 1));
            raycastDirs[4] = raycastOrigin.TransformDirection(new Vector3(-1, 0, 0));

            for (int i = 0; i < raycastHits.Length; i++)
            {
                Physics.Raycast(raycastOrigin.position, raycastDirs[i], out raycastHits[i], Mathf.Infinity, layerMask);
                distances[i] = raycastHits[i].distance;
                Debug.DrawRay(raycastOrigin.position, raycastDirs[i]);
            }
        }
    }
}