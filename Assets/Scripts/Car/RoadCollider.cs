﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadCollider : MonoBehaviour
{
    private delegate void RoadColliderAdding();
    private static event RoadColliderAdding RoadColliderAdded;

    private static List<RoadCollider> instances = new List<RoadCollider>();

    [SerializeField] private new Collider collider;
    [SerializeField] private int order;

    private float spaceTaken;
    private Vector3 localExtends;

    private static bool roadColliderAddedWasCalled;

    private void Awake()
    {
        roadColliderAddedWasCalled = false;
        RoadColliderAdded += RoadCollider_RoadColliderAdded;
        instances.Add(this);
        localExtends = Quaternion.Euler(-transform.eulerAngles) * collider.bounds.extents;
        for (int i = 0; i < 3; i++)
            localExtends[i] = Mathf.Abs(localExtends[i]);
        RoadColliderAdded();

        //localExtends = Quaternion.Euler(-transform.eulerAngles) * collider.bounds.extents;
        //for (int i = 0; i < 3; i++)
        //    localExtends[i] = Mathf.Abs(localExtends[i]);
    }

    private void OnDestroy()
    {
        RoadColliderAdded -= RoadCollider_RoadColliderAdded;
        instances.Remove(this);
    }

    public static void GetProgress(Transform object_, ref float progress_)
    {
        bool IsInArea(Vector3 pos_, Bounds bounds_)
        {
            bool IsInRange(float value_, float a_, float b_)
            {
                float _minVal = Mathf.Min(a_, b_);
                float _maxVal = _minVal == a_ ? b_ : a_;
                return value_ > _minVal && value_ < _maxVal;
            }

            bool _x = IsInRange(pos_.x, bounds_.center.x - bounds_.extents.x, bounds_.center.x + bounds_.extents.x);
            bool _y = IsInRange(pos_.y, bounds_.center.y - bounds_.extents.y, bounds_.center.y + bounds_.extents.y);
            bool _z = IsInRange(pos_.z, bounds_.center.z - bounds_.extents.z, bounds_.center.z + bounds_.extents.z);

            return _x && _y && _z;
        }

        int i = 0;

        for (; i < instances.Count; i++)
            if (IsInArea(object_.position, instances[i].collider.bounds))
                break;
        if (i >= instances.Count) return;
        float _objectZ = instances[i].transform.InverseTransformPoint(object_.position).z;
        float _start = instances[i].transform.InverseTransformPoint(instances[i].collider.bounds.center - instances[i].localExtends).z;
        float _end = instances[i].transform.InverseTransformPoint(instances[i].collider.bounds.center + instances[i].localExtends).z;

        float _prevSpaces = 0;
        //Debug.Log(i);
        for (int j = 0; j < i; j++)
            _prevSpaces += instances[j].spaceTaken;

        progress_ = ((_objectZ / (_end + _start)) * instances[i].spaceTaken) + _prevSpaces;
    }

    private void RoadCollider_RoadColliderAdded()
    {
        instances.Sort((o, p) => o.order.CompareTo(p.order));
        if (!roadColliderAddedWasCalled) CountSpaces();
        roadColliderAddedWasCalled = true;
    }

    private void CountSpaces()
    {
        float _total = 0;

        foreach (RoadCollider rc in instances)
            _total += rc.localExtends.z * 2;

        foreach (RoadCollider rc in instances)
            rc.spaceTaken = rc.localExtends.z * 2 / _total;
    }
}