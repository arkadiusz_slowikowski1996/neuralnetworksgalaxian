﻿using GeneticNeuralNetwork;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Car
{
    public abstract class Manager : BaseManager
    {
        private static Manager instance;

        [SerializeField] protected Brain brainPrefab;
        [SerializeField] protected Transform brainDefaultPosition;
        [SerializeField] protected Transform brainTransform;
        [SerializeField] protected Transform brainTarget;
        [SerializeField] protected GameObject myMap;
        [SerializeField] protected Text debugText;
        [SerializeField] private int brainsPopulation;
        [SerializeField] protected int NNinputs;
        [SerializeField] protected int NNoutputs;
        [SerializeField] protected int NNhiddenLayers;
        [SerializeField] protected int NNhiddenNeurons;
        [SerializeField] private float timeScale = 1;

        protected int currGeneration;

        public static int BrainsPopulation { get { return instance.brainsPopulation; } }

        protected void Awake()
        {
            instance = this;
        }

        public static void BrainDied(int index)
        {
            instance.HandleBrainDied(index);
        }

        protected virtual void Update()
        {
            Time.timeScale = timeScale;
        }

        protected abstract void HandleBrainDied(int index);
    }
}