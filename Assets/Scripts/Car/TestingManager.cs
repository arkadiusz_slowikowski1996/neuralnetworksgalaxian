﻿using GeneticNeuralNetwork;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Car
{
    public class TestingManager : Manager
    {
        private static TestingManager instance;

        private GeneticNeuralNetwork.Brain mainBrain;

        private new void Awake()
        {
            base.Awake();
            instance = this;
        }

        private void _Start()
        {
            base.Awake();
        }

        protected override void Update()
        {
            base.Update();
        }

        private void SpawnBrain()
        {
            mainBrain = Instantiate(brainPrefab);
            mainBrain.network = new NeuralNetwork();
            mainBrain.network.CopyNetworkFrom(GeneticManager.theBestNetwork);
            mainBrain.transform.position = brainDefaultPosition.position;
            mainBrain.transform.rotation = brainDefaultPosition.rotation;
        }

        protected override void HandleBrainDied(int index)
        {
            SpawnBrain();
        }

        public override void OnActivated()
        {
            myMap.SetActive(true);
            NeuralNetwork.Setup(NNinputs, NNoutputs, NNhiddenLayers, NNhiddenNeurons);
            SpawnBrain();
            _Start();
        }

        public override void OnDeactivated()
        {
            if (mainBrain) Destroy(mainBrain.gameObject);
            myMap.SetActive(false);
        }
    }
}