﻿using GeneticNeuralNetwork;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Car
{
    public class TrainingManager : Manager
    {
        private static TrainingManager instance;

        [SerializeField] private int numberOfParents = 1;
        [SerializeField] private int numberOfMutations = 1;
        private float bestTimeSoFar = float.MaxValue;

        private static int NumberOfDead
        {
            get
            {
                int returnValue = 0;
                foreach (bool b in isDead)
                    if (b)
                        returnValue++;
                return returnValue;
            }
        }
        private static bool[] isDead;
        public static Transform BrainTarget { get { return instance.brainTarget; } }
        internal static int NNInputs { get { return instance.NNinputs; } }

        private new void Awake()
        {
            base.Awake();
            instance = this;
        }

        private void _Start()
        {
            base.Awake();
            GeneticManager.Setup(numberOfParents, numberOfMutations);
            NeuralNetwork.Setup(NNinputs, NNoutputs, NNhiddenLayers, NNhiddenNeurons);
            currGeneration++;
            foreach (CarBrain cb in GeneticManager.CreateNewGeneration(instance.brainPrefab, BrainsPopulation))
            {
                cb.transform.position = instance.brainDefaultPosition.position;
                cb.transform.rotation = instance.brainDefaultPosition.rotation;
                cb.transform.parent = instance.brainTransform;
            }

            isDead = new bool[BrainsPopulation];
        }

        protected override void Update()
        {
            base.Update();

            debugText.text = string.Format("Generation: {0}, Best time so far: {1}", currGeneration, bestTimeSoFar);
        }

        protected override void HandleBrainDied(int index)
        {
            isDead[index] = true;

            if (NumberOfDead == BrainsPopulation)
            {
                currGeneration++;
                foreach (CarBrain cb in GeneticManager.currBrains)
                    if (cb.finished && cb.time < bestTimeSoFar)
                        bestTimeSoFar = cb.time;

                foreach (CarBrain cb in GeneticManager.CreateNewGeneration(instance.brainPrefab, BrainsPopulation))
                {
                    cb.transform.position = instance.brainDefaultPosition.position;
                    cb.transform.parent = instance.brainTransform;
                }
                isDead = new bool[BrainsPopulation];
            }
        }

        public override void OnActivated()
        {
            _Start();
            myMap.SetActive(true);
        }

        public override void OnDeactivated()
        {
            currGeneration = 0;
            GeneticManager.DestoryBrainObjects();
            myMap.SetActive(false);
        }
    }
}