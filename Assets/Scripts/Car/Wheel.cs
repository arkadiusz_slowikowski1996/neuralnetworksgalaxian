﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Car
{
    public class Wheel : MonoBehaviour
    {
        public WheelCollider wheelCollider;
        [SerializeField] private Transform mesh;

		private void Start()
		{
			wheelCollider.ConfigureVehicleSubsteps(5, 12, 15);
		}

		private void LateUpdate()
        {
            Vector3 _pos = Vector3.zero;
            Quaternion _rot = Quaternion.identity;
            wheelCollider.GetWorldPose(out _pos, out _rot);
            mesh.transform.rotation = _rot;
            mesh.transform.position = _pos;
        }
    }
}