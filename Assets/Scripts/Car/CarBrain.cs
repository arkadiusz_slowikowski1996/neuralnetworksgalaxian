﻿using GeneticNeuralNetwork;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Car
{
    public class CarBrain : GeneticNeuralNetwork.Brain
    {
        public float time;
        public float distance;

        internal bool finished;

        private float Distance
        {
            get
            {
                NavMeshPath path = new NavMeshPath();
                NavMesh.CalculatePath(transform.position, TrainingManager.BrainTarget.position, NavMesh.AllAreas, path);
                float _returnValue = 0;
                for (int i = 1; i < path.corners.Length; i++)
                    _returnValue += Vector3.Distance(path.corners[i - 1], path.corners[i]);

                return _returnValue;
            }
        }


        [SerializeField] private Raycasting raycasting;
        [SerializeField] public Vector3 dest;
        [SerializeField] private Wheel[] wheels;
        [SerializeField] private float maxSteerForce = 45;
        [SerializeField] private float maxThrottle = 45;

        private Vector3 prevPos;
        private Vector3 pos5SecAgo;

        private void Start()
        {
            prevPos = transform.position;
            StartCoroutine(KillWhenIdle());
            StartCoroutine(KillWhenRotating());
        }

        private void Update()
        {
            time += Time.deltaTime;
            //navMeshAgent.Warp(new Vector3(transform.position.x, 0, transform.position.z));
            //dest = navMeshAgent.destination;
            distance = Distance;
            //RoadCollider.GetProgress(transform, ref distance);
        }

        private void FixedUpdate()
        {
            double[] _inputs = new double[TrainingManager.NNInputs];
            try
            {
                for (int i = 0; i < _inputs.Length; i++)
                    _inputs[i] = raycasting.raycastHits[i].distance > 10 ? 1 : raycasting.raycastHits[i].distance / 10;//Mathf.Clamp(raycasting.raycastHits[i].distance / 10, -1, 1);

                double[] _outputs = network.Process(_inputs);
                SetSteer((float)_outputs[0]);
                SetThrottle((float)_outputs[1]);
            }
            catch { }
        }

        public void SetSteer(float value)
        {
            value = Mathf.Clamp(value, -1, 1);

            for (int i = 0; i < wheels.Length / 2; i++)
                wheels[i].wheelCollider.steerAngle = maxSteerForce * value;
        }

        public void SetThrottle(float value)
        {
            value = Mathf.Clamp(value, -1, 1);

            for (int i = 0; i < wheels.Length / 2; i++)
            {
                if (value > 0) wheels[i].wheelCollider.motorTorque = value * maxThrottle * Time.deltaTime;
                if (value < 0) wheels[i].wheelCollider.motorTorque = value * maxThrottle * Time.deltaTime;
            }
        }

        internal void TurnOff(bool finished)
        {
            this.finished = finished;
            ChangeScore(-Distance + (finished ? time : 0));
            gameObject.SetActive(false);
            TrainingManager.BrainDied(transform.GetSiblingIndex());
        }

        private IEnumerator KillWhenIdle()
        {
            yield return new WaitForSeconds(3);

            while (true)
            {
                if (Vector3.Distance(prevPos, transform.position) < 0.2f)
                {
                    TurnOff(false);
                    TrainingManager.BrainDied(transform.GetSiblingIndex());
                }
                prevPos = transform.position;
                yield return new WaitForSeconds(1);
            }
        }

        private IEnumerator KillWhenRotating()
        {
            yield return new WaitForSeconds(10);

            while (true)
            {
                if (Vector3.Distance(pos5SecAgo, transform.position) < 7)
                {
                    TurnOff(false);
                    TrainingManager.BrainDied(transform.GetSiblingIndex());
                }
                pos5SecAgo = transform.position;
                yield return new WaitForSeconds(5);
            }
        }
    }
}