﻿using UnityEngine;

public class MainMenu_Ctrl : MonoBehaviour
{
    [SerializeField] private BaseManager trainingGO;
    [SerializeField] private BaseManager testingGO;
    [SerializeField] private GameObject trainingButton;
    [SerializeField] private GameObject testingButton;
    [SerializeField] private GameObject backToMenuButton;

    public void Train_ButtonClicked()
    {
        trainingGO.gameObject.SetActive(true);
        trainingGO.OnActivated();
        SwitchButtons(false);
    }

    public void Test_ButtonClicked()
    {
        testingGO.gameObject.SetActive(true);
        testingGO.OnActivated();
        //loadButton.SetActive(true);
        SwitchButtons(false);
    }

    public void BackToMenu_ButtonClicked()
    {
        trainingGO.OnDeactivated();
        trainingGO.gameObject.SetActive(false);
        testingGO.OnDeactivated();
        testingGO.gameObject.SetActive(false);
        SwitchButtons(true);
    }

    private void SwitchButtons(bool b_)
    {
        trainingButton.SetActive(b_);
        testingButton.SetActive(b_);
        backToMenuButton.SetActive(!b_);
    }
}