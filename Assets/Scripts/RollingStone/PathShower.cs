﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathShower : MonoBehaviour
{
    [SerializeField] private Transform start;
    [SerializeField] private Transform end;
    [SerializeField] private NavMeshAgent navMeshAgent;
    [SerializeField] private LineRenderer lineRenderer;

    private Vector3 startPos;
    private Vector3 endPos;
    private Vector3[] pathCorners;
    private NavMeshPath navMeshPath = null;

    private void Awake()
    {
        startPos = start.position;
        endPos = end.position;
        navMeshAgent.SetDestination(end.position);
        navMeshPath = navMeshAgent.path;
        navMeshAgent.CalculatePath(endPos, navMeshPath);
    }

    private void Start()
    {
        lineRenderer.positionCount = navMeshPath.corners.Length;
        lineRenderer.SetPositions(navMeshPath.corners);
    }
}