﻿using UnityEngine;

namespace GeneticNeuralNetwork
{
    public abstract class Brain : MonoBehaviour
    {
        internal NeuralNetwork network;
        internal double score;

        protected void ChangeScore(double delta) => score += delta;
    }
}