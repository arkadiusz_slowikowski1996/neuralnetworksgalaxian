﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GeneticNeuralNetwork
{
    [Serializable]
    public class NeuralNetwork
    {
        internal static int inputs;
        internal static int outputs;
        internal static int hiddenLayers;
        internal static int hiddenNeuronsPerLayer;

        private int[] layers;
        private double[] weights;

        public NeuralNetwork() { }

        internal NeuralNetwork(object data)
        {
            InitNetwork();

            for (int i = 0; i < weights.Length; i++)
                weights[i] = Random.Range(-1f, 1f);
        }

        internal NeuralNetwork(NeuralNetwork[] parents, int numberOfMutations, int parentToCopy = -1)
        {
            InitNetwork();

            for (int i = 0; i < weights.Length; i++)
                weights[i] = parents[parentToCopy == -1 ? Random.Range(0, parents.Length) : parentToCopy].weights[i];

            if (parentToCopy == -1)
                for (int i = 0; i < numberOfMutations; i++)
                    weights[Random.Range(0, weights.Length)] = Random.Range(-1f, 1f);
        }

        public static void Setup(int inputs, int outputs, int hiddenLayers, int hiddenNeuronsPerLayer)
        {
            NeuralNetwork.inputs = inputs;
            NeuralNetwork.outputs = outputs;
            NeuralNetwork.hiddenLayers = hiddenLayers;
            NeuralNetwork.hiddenNeuronsPerLayer = hiddenNeuronsPerLayer;
        }

        public void CopyNetworkFrom(NeuralNetwork neuralNetwork)
        {
            InitNetwork();

            for (int i = 0; i < weights.Length; i++)
                weights[i] = neuralNetwork.weights[i];
        }

        private void InitNetwork()
        {
            layers = new int[2 + hiddenLayers];
            for (int i = 0; i < layers.Length; i++)
            {
                if (i == 0)
                    layers[i] = inputs;
                else if (i == layers.Length - 1)
                    layers[i] = outputs;
                else
                    layers[i] = hiddenNeuronsPerLayer;
            }

            int weightsNum = 0;
            for (int i = 1; i < layers.Length; i++)

                weightsNum += layers[i - 1] * layers[i];

            weights = new double[weightsNum];
        }

        public double[] Process(double[] inputs)
        {
            int weightsIndex = 0;
            double[] prevLayer = inputs;

            for (int i = 1; i < layers.Length; i++)
            {
                double[] currentLayer = new double[layers[i]];

                for (int j = 0; j < layers[i]; j++)
                {
                    double sum = 0;

                    for (int h = 0; h < prevLayer.Length; h++)
                    {
                        sum += prevLayer[h] * weights[weightsIndex];
                        weightsIndex++;
                    }

                    currentLayer[j] = Math.Tanh(sum);
                }

                prevLayer = currentLayer;
            }

            return prevLayer;
        }
    }
}