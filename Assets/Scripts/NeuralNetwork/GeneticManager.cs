﻿using Object = UnityEngine.Object;

namespace GeneticNeuralNetwork
{
    public static class GeneticManager
    {
        public static NeuralNetwork theBestNetwork;
        public static Brain[] currBrains;
        private static int numberOfParents = 2;
        private static int numberOfMutations = 1;
        private static int currentGeneration = 0;

        public static void Setup(int numberOfParents, int numberOfMutations)
        {
            GeneticManager.numberOfParents = numberOfParents;
            GeneticManager.numberOfMutations = numberOfMutations;
        }
        public static Brain[] CreateNewGeneration(Brain brainPrefab, int population)
        {
            currBrains = currentGeneration == 0 ? SpawnBrains(brainPrefab, population) : Reproduce(brainPrefab, population);
            currentGeneration++;
            return currBrains;
        }
        public static void DestoryBrainObjects()
        {
            KillBrains(currBrains);
        }
        private static void KillBrains(Brain[] brains)
        {
            if (brains != null)
            {
                int population = brains.Length;

                for (int i = 0; i < population; i++)
                    if (brains[i]) Object.Destroy(brains[i].transform.gameObject);

                brains = null;
            }
        }
        private static Brain[] SpawnBrains(Brain brainPrefab, int population, object dataToSpawnFrom = null)
        {
            currBrains = new Brain[population];
            for (int i = 0; i < population; i++)
                currBrains[i] = SpawnBrain(brainPrefab, dataToSpawnFrom, -1);

            return currBrains;
        }
        private static Brain SpawnBrain(Brain brainPrefab, object data = null, int parentToCopy = -1)
        {
            Brain currBrain = Object.Instantiate(brainPrefab);

            if (data is NeuralNetwork[])
            {
                currBrain.network = new NeuralNetwork((NeuralNetwork[])data, numberOfMutations, parentToCopy);
            }
            else if (data is null)
            {
                currBrain.network = new NeuralNetwork(data);
            }

            return currBrain;
        }
        private static Brain[] Reproduce(Brain brainPrefab, int population)
        {
            Brain[] brainsToKill = new Brain[currBrains.Length];
            for (int i = 0; i < brainsToKill.Length; i++)
                brainsToKill[i] = currBrains[i];

            NeuralNetwork[] parents = ChooseParents();

            if (parents != null) theBestNetwork = parents[0];
            for (int i = 0; i < population; i++)
                currBrains[i] = SpawnBrain(brainPrefab, parents, i < numberOfParents && parents != null ? i : -1);

            KillBrains(brainsToKill);

            return currBrains;
        }
        private static NeuralNetwork[] ChooseParents()
        {
            NeuralNetwork[] parents = new NeuralNetwork[numberOfParents];

            for (int i = 0; i < parents.Length; i++)
            {
                double maxValue = double.MinValue;
                int maxIndex = -1;

                for (int j = 1; j < currBrains.Length; j++)
                {
                    if (currBrains[j].score > maxValue)
                    {
                        maxIndex = j;
                        maxValue = currBrains[j].score;
                    }
                }
                currBrains[maxIndex].score /= 2;
                parents[i] = currBrains[maxIndex].network;
            }

            return parents;
        }
    }
}