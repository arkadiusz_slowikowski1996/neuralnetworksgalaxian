﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadGames : MonoBehaviour
{
    public void LoadGame(int index)
    {
        SceneManager.LoadScene(index + 1);
    }
}