﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ImportantObjects_Plugin : EditorWindow
{
    private List<Object> objects = new List<Object>();

    [MenuItem("Peon's Plugins/Important Objects")]
    private static void Start()
    {
        GetWindow(typeof(ImportantObjects_Plugin)).Show();
    }

    private void OnGUI()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            EditorGUILayout.LabelField(i.ToString() + ": ");

            objects[i] = EditorGUILayout.ObjectField(objects[i], typeof(Object), true);

            if (GUILayout.Button("X"))
            {
                objects.RemoveAt(i);
                continue;
            }
        }

        if (GUILayout.Button("Add object"))
        {
            objects.Add(null);
        }
    }
}

#endif