﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InGameDebug
{
	[Serializable]
	public class Settings
	{
		public bool showDebug = true;
		public float colorAlpha = 1;
		public float entryLifespan = 3;
		public int fontSize = 21;
		public bool registerLogs = true;
		public bool registerErrors = true;
		public bool registerWarnings = true;
	}
}