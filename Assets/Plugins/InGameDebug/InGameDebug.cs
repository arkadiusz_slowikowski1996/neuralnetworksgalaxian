﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace InGameDebug
{
	public class InGameDebug : MonoBehaviour
	{
		[SerializeField] private Settings settings;

		[SerializeField] private Text entryPrefab;
		[SerializeField] private Transform entriesParent;
		[SerializeField] private float entryLifespan = 1;
		[SerializeField] private Color logColor;
		[SerializeField] private Color errorColor;
		[SerializeField] private Color warningColor;

		private List<Text> currEntries = new List<Text>();

		private Queue<string> allLogs = new Queue<string>();

		private void Awake()
		{
			Application.logMessageReceived += Application_logMessageReceived;

			ReadSettings();
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Alpha1))
				Debug.Log("Log");
			if (Input.GetKeyDown(KeyCode.Alpha2))
				Debug.LogError("Error");
			if (Input.GetKeyDown(KeyCode.Alpha3))
				Debug.LogWarning("Warning");

			if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.B))
			{
				if (!settings.showDebug)
				{
					settings.showDebug = true;
					Debug.Log("Debug enabled");
				}
				else
				{
					Debug.Log("Debug disabled");
					settings.showDebug = false;
				}
			}
		}

		private void OnApplicationQuit()
		{
			StringBuilder _sb = new StringBuilder();

			while (allLogs.Count > 0)
				_sb.Append(allLogs.Dequeue() + "\n");

			File.WriteAllText(Application.dataPath + @"\DebugLog.txt", _sb.ToString());
		}

		private void ReadSettings()
		{
			if (File.Exists(Application.dataPath + "/InGameDebugSettings.txt"))
			{
				settings = JsonUtility.FromJson<Settings>(File.ReadAllText(Application.dataPath + "/InGameDebugSettings.txt"));
			}
			else
			{
				string _json = JsonUtility.ToJson(settings);
				File.WriteAllText(Application.dataPath + "/InGameDebugSettings.txt", _json);
			}
		}

		private void Application_logMessageReceived(string condition, string stackTrace, LogType type)
		{
			SpawnEntry(condition, stackTrace, type);
		}

		private void SpawnEntry(string condition, string stackTrace, LogType type)
		{
			if (!settings.showDebug) return;

			Color _colorToSet = Color.white;

			switch (type)
			{
				case LogType.Assert:
				case LogType.Error:
				case LogType.Exception:
					if (!settings.registerErrors) return;
					_colorToSet = errorColor;
					break;
				case LogType.Warning:
					if (!settings.registerWarnings) return;
					_colorToSet = warningColor;
					break;
				case LogType.Log:
					if (!settings.registerLogs) return;
					_colorToSet = logColor;
					break;
			}
			_colorToSet.a = settings.colorAlpha;

			Text _currEntry = Instantiate(entryPrefab, entriesParent);
			_currEntry.text = string.Format("<size={0}>{1}</size> || {2}", (2f * settings.fontSize), condition, stackTrace);
			_currEntry.fontSize = settings.fontSize;
			_currEntry.rectTransform.sizeDelta = new Vector2(_currEntry.rectTransform.sizeDelta.x, Mathf.Ceil(((float)settings.fontSize / 21f) * 75f));

			_currEntry.color = _colorToSet;

			currEntries.Add(_currEntry);
			allLogs.Enqueue(string.Format("[{0}]  type: {1} || {2} || {3}", Time.time, type, condition, stackTrace));

			MoveAllEntries();

			StartCoroutine(DestroyEntry(_currEntry));
		}

		private void MoveAllEntries()
		{
			float _entryHeight = currEntries[0].rectTransform.sizeDelta.y;

			for (int i = 0; i < currEntries.Count; i++)
			{
				currEntries[i].transform.localPosition = new Vector3(
					0,
					_entryHeight * (currEntries.Count - i - 1),
					0
				);
			}
		}

		private IEnumerator DestroyEntry(Text entry_)
		{
			yield return new WaitForSeconds(entryLifespan);
			currEntries.Remove(entry_);
			Destroy(entry_.gameObject);
		}
	}
}