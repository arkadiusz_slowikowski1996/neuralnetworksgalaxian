﻿#if UNITY_EDITOR

using UnityEditor;

public class OpenGIT : EditorWindow
{
	[MenuItem("Peon's Plugins/OpenGIT %#&g")]
	private static void OpenGit()
	{
		System.Diagnostics.Process.Start(@"C:\Program Files\Git\git-bash.exe");
	}
}

#endif